# WEB-SCRAPER

##### Author: Kent Joash A. Zamudio
##### Date ended: July 19, 2021

### Description

Webscraper or data crawler for list of barangays in the Philippines.
This project uses Nodejs with axios, request, and fast-csv modules for sending requests and processing responses.

### Wiki
Visit the wiki page for more information about the functions used in this program


## NOTE: Stretch Goal #2 not completed
