const axios = require('axios'); // module used for sending http requests
const path = require('path'); // module used for resolving directory-related queries
const { writeToPath } = require('@fast-csv/format'); // module used for exporting object array to csv format
const appendUrl = require('./appendUrl')


/**
 * This function sends the http request through the axios module, specifically the axios.get() function
 * @param {string} startUrl - A request URL string that is needed for axios.get() function
 * @param {string} [province=Antique] - the province from which the desired list of barangays belong to
 * @param {string} [municipality=Culasi] - the municipality from which the desired list of barangays belong to
 * @return {Promise} - A promise object containing the array of provinces/municipalities/barangays depending on the sent param
 */
function promiseBrgy(startUrl, province, municipality) {
		axios
			.get(startUrl)
			.then(provinces => {
				if (provinces === null) {
					throw 'List of provinces not Found';
				}
				return axios.get(appendUrl(startUrl, provinces.data.module, province));
			})
			.then(municipalities => {
				if (municipalities === null) {
					throw 'List of municipalities not Found';
				}
				return axios.get(appendUrl(startUrl, municipalities.data.module, municipality));
			})
			.then(barangays => {
				if (barangays === null) {
					throw 'List of barangays not Found';
				}
				// console.log(barangays);
				const brgyList = [...barangays.data.module].map(
					({ nameLocal, displayName, ...rest }) => rest
				);
				writeToPath(
					path.resolve(__dirname, '../output', 'promise-result.csv'),
					brgyList,
					{ headers: ['id', 'name', 'parentId'] }
				)
					.on('error', err => console.error(err))
					.on('finish', () => console.log('PROMISE Implementation: Done writing csv file (promise-result.csv).'));
			})
			.catch(error => {
				console.log(error);
			});
}

// export promiseBrgy function
module.exports = promiseBrgy