const axios = require('axios'); // module used for sending http requests
const path = require('path'); // module used for resolving directory-related queries
const { writeToPath } = require('@fast-csv/format'); // module used for exporting object array to csv format
const appendUrl = require('./appendUrl'); // 

/**
 * This async function sends the http request through the axios module, specifically the axios.get() function,
 * 		it follows the sequence provinces->municipalities->barangays for each succeeding http request in order to get the list of brgy
 * @param {string} startUrl - A request URL string that is needed for axios.get() function
 * @param {string} [province=Antique] - the province from which the desired list of barangays belong to
 * @param {string} [municipality=Culasi] - the municipality from which the desired list of barangays belong to
 * @return {undefined}
 */
async function awaitBrgy(startUrl, province='Antique', municipality='Culasi') {
	try{
		// get list of provinces
		const provinces = await axios.get(startUrl);
		if(provinces.data.module===null){throw `List of provinces not found`}

		// get list of municipalities
		const municipalitiesUrl = appendUrl(startUrl, provinces.data.module, province);
		const municipalities = await axios.get(municipalitiesUrl);
		if(municipalities.data.module===null){throw `List of municipalities in ${province} not found`}

		// get list of barangays
		const brgyUrl = appendUrl(startUrl, municipalities.data.module, municipality);
		const brgyList = await axios.get(brgyUrl);
		if(brgyList.data.module===null){throw `List of barangays in ${municipality} not found`}

		// filter the details so id, name, and parentId will be the only keys to remain
		const filteredList = [...brgyList.data.module].map(
					({ nameLocal, displayName, ...rest }) => rest
				);

		// write to csv
		writeToPath(
			path.resolve(__dirname, '../output', 'async-result.csv'),
			filteredList,
			{ headers: ['id', 'name', 'parentId'] }
		)
			.on('error', err => console.error(err))
			.on('finish', () => 
				console.log('ASYNC/AWAIT Implementation: Done writing csv file (async-result.csv).')
			);
	}catch(error){
		console.log(error);
	}
} 


module.exports = awaitBrgy;