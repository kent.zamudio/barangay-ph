const request = require('request'); // module used for sending http requests
const appendUrl = require('./appendUrl'); // module used for appending addressId of the municipality/province

// const province = 'Antique';
// const municipality = 'Culasi';
// const lazadaUrl = 'https://member.lazada.com.ph/locationtree/api/getSubAddressList?countryCode=PH';

/**
 * @deprecated removed in the refactored implementation
 * this function serves as the callback function for the requestBrgy()
 * @callback processResponse
 * @param {Error} error - Contains the error that occured during the request process
 * @param {JSON} body - The response of the request made
 * @returns {undefined}
 */
function processResponse(error, body) {
	if (error) {
		console.log(`ERROR: ${error.reason}`);
	} else {
		// parse the response body
		const parsed = JSON.parse(body);
		// get the value of key module
		// key module containts the object array of barangays
		let { module: result } = parsed;

		// if the value of module is null, it may be a non-existent `addressId` in the url
		if (result === null) {
			console.log('ADDRESS NOT FOUND');
			return;
		}
		// use map to remove nameLocal and displayName, the remaining keys will be 'id, name, parentId'
		const brgyList = result.map(({ nameLocal, displayName, ...rest }) => rest);
		console.log(brgyList);
	}
}

/**
 * @deprecated refactored
 * This function sends the http requests and process the response until the desired list of barangays is printed in the console
 * @param {string} url - The initial URL for crawling, the function appends `addressId` after each request calls
 * @param {string} [province=Antique] - The province from which to the barangay belongs to
 * @param {string} [municipality=Culasi] - The municipality from which to the barangay belongs to
 * @return {undefined}
 */
function requestBrgy_v1(
	url,
	province = 'Antique',
	municipality = 'Culasi'
) {

	/**
	 * Sends request to collect objects containing details of each province in the Philippines
	*/
	request(
		{
			method: 'GET',
			uri: url,
			headers: {
				'Content-Type': 'application/json',
				'dataType': 'json'
			}
		},
		(error, response, body) => {
			if (error) {
				processResponse(error, body);
				return;
			}
			let result = JSON.parse(body);
			const { module: provinceList } = result;

			const requestMunicipalities = appendUrl(url,provinceList, province);

			/**
			 * Sends request to collect objects containing details of each municipality in the given province
			*/
			request(
				{
					method: 'GET',
					uri: requestMunicipalities,
					headers: {
						'Content-Type': 'application/json',
						'dataType': 'json'
					}
				},
				(error, response, body) => {
					if (error) {
						processResponse(error, body);
						return;
					}

					let result = JSON.parse(body);
					const { module: municipalityList } = result;

					const requestBrgyList = appendUrl(url, municipalityList, municipality);

					/**
					 * Sends request to collect objects containing details of each barangay in the municipality
					*/
					request(
						{
							method: 'GET',
							uri: requestBrgyList,
							headers: {
								'Content-Type': 'application/json',
								'dataType': 'json'
							}
						},
						(error, response, body) => {
							if (error) {
								processResponse(error, body);
								return;
							}

							processResponse(null, body);
						});
				});
		});
}




// export functions
module.exports = requestBrgy_v1 ;


// requestBrgy(lazadaUrl, success, failed)
// console.log(module)
