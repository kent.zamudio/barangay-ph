const axios = require('axios'); // module used for sending http requests
const path = require('path'); // module used for resolving directory-related queries
const { writeToPath } = require('@fast-csv/format'); // module used for exporting object array to csv format
const appendUrl = require('../appendUrl'); // module used for exporting object array to csv format

const province = 'Antique';
const municipality = 'Culasi';

const lazadaUrl =
	'https://member.lazada.com.ph/locationtree/api/getSubAddressList?countryCode=PH';


async function awaitBrgy_v1(starturl, province, municipality) {
	await axios.get(starturl)
		.then((response) => {
			const provinces = response.data.module; 
			if (provinces===undefined) {throw 'List of provinces not found'}
			const municipalitiesUrl = appendUrl(starturl, provinces, province);

			return axios.get(municipalitiesUrl);
		}).then((response) => {
			const municipalities = response.data.module;
			if (municipalities===undefined) {throw 'List of municipalities not found';}
			const barangaysUrl = appendUrl(starturl,municipalities, municipality);
			return axios.get(barangaysUrl);
		}).then((response) => {
			const barangays = response.data.module;
			if (barangays===undefined) {throw 'List of barangays not found';}
			const brgyList = [...barangays].map(
					({ nameLocal, displayName, ...rest }) => rest
				);
				writeToPath(
					path.resolve(__dirname, '../../output', 'async-result.csv'),
					brgyList,
					{ headers: ['id', 'name', 'parentId'] }
				)
					.on('error', err => console.error(err))
					.on('finish', () => console.log('ASYNC/AWAIT Implementation: Done writing csv file (async-result.csv).'));
		}).catch((error)=> {
			console.log(error)
		})

} 


module.exports = awaitBrgy_v1