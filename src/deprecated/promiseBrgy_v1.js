const axios = require('axios'); // module used for sending http requests
const path = require('path'); // module used for resolving directory-related queries
const { writeToPath } = require('@fast-csv/format'); // module used for exporting object array to csv format


/*
 * This function appends the addressId of the province/municipality, depending in the `place` param, to the `url` 
 * @param {string} url - The request URL to which we append the `addressId`
 * @param {object} data - The array of objects that contains the list of all provinces or municipalities in a province
 * @param {string} place - Either the name of municipality or province from which the barangay we desire to get belongs to
 * @return {string} - A request URL with the `addressId` of province/municipality appended to it
 */
function appendUrl(url, data, place) {
	const placeDetails = data.filter(placeObj => placeObj.name === place)[0];
	const appendUrl = `${url}&addressId=${placeDetails.id}`;
	return appendUrl; 
}

/**
 * @deprecated This function is removed in the new implementation
 * This function sends the http request through the axios module, specifically the axios.get() function
 * @param {string} url - A request URL string that is needed for axios.get() function
 * @return {Promise} - A promise object containing the array of provinces/municipalities/barangays depending on the sent param
 */
function axiosGetRequest(url) {
	return new Promise((success, fail) => {
		axios
			.get(url)
			.then(response => {
				success(response.data.module);
			})
			.catch(error => {
				throw error;
			});
	});
}

/**
 * @deprecated this is an older implementation
 * This function collects the starting request URL, name of province, and name of municipality, 
 * 		then processes the requests until the list of barangays is collected, then exports it to `promise-result.csv`
 * @param {string} startUrl - the request URL for data crawling
 * @param {string} [province=Antique] - the province from which the desired list of barangays belong to
 * @param {string} [municipality=Culasi] - the municipality from which the desired list of barangays belong to
 * @return {undefined}
 */
function promiseBrgy_v1(startUrl, province = 'Antique', municipality = 'Culasi') {
	axiosGetRequest(startUrl)
		.then(provinces => {
			if (provinces === null) {
				throw 'List of provinces not Found';
			}
			return axiosGetRequest(appendUrl(startUrl, provinces, province));
		})
		.then(municipalities => {
			if (municipalities === null) {
				throw 'List of municipalities not Found';
			}
			return axiosGetRequest(appendUrl(startUrl, municipalities, municipality));
		})
		.then(barangays => {
			if (barangays === null) {
				throw 'List of barangays not Found';
			}
			// console.log(barangays);
			const brgyList = [...barangays].map(
				({ nameLocal, displayName, ...rest }) => rest
			);
			writeToPath(
				path.resolve(__dirname, '../../output', 'promise-result.csv'),
				brgyList,
				{ headers: ['id', 'name', 'parentId'] }
			)
				.on('error', err => console.error(err))
				.on('finish', () => console.log('Done writing.'));
		})
		.catch(error => {
			console.log(error);
		});
}

// export function
module.exports = promiseBrgy_v1