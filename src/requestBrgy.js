const request = require('request'); // module used for sending http requests
const appendUrl = require('./appendUrl'); // module used for appending addressId of the municipality/province

/**
 * This function sends the requests through the request module
 * @returns {Object}
 */
function getRequest(url, callback, data) {
  request({
      method: 'GET',
      uri: url,
      headers: {
        'Content-Type': 'application/json',
        'dataType': 'json'}
    }, function (error, response, body){
      if(callback) {
          callback(error, JSON.parse(body))
      }
    })
}


/**
 * This function facilitates the whole process of sending requests and processing the response
 * @param {string} url - The initial URL for crawling, the function appends `addressId` after each request calls
 * @param {string} [province=Antique] - The province from which to the barangay belongs to
 * @param {string} [municipality=Culasi] - The municipality from which to the barangay belongs to
 * @return {undefined}
 */
function requestBrgy(url, province='Antique', municipality='Culasi'){
	// get List of provinces
	getRequest(url, (error, body)=>{
		if (error){
			console.log(error);
			return;
		}
		const { module: provinceList } = body;
		const requestMunicipalities = appendUrl(url, provinceList, province);

		// get list of municipalities
		getRequest(requestMunicipalities,(error, body)=>{
			if (error){
				console.log(error);
				return;
			}
				const { module: municipalityList } = body;
			const requestBrgyList = appendUrl(url, municipalityList, municipality);

			// get list of brgy
			getRequest(requestBrgyList,(error, body)=>{
				if (error){
					console.log(error);
					return;
				}
						const { module: brgyList } = body;
				console.log(brgyList)
				
			})
		})

	})
}



// export functions
module.exports = requestBrgy ;

// requestBrgy(lazadaUrl)
// console.log(module)
