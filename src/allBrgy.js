const axios = require('axios'); // module used for sending http requests
const path = require('path'); // module used for resolving directory-related queries
const { writeToPath } = require('@fast-csv/format'); // module used for exporting object array to csv format
const appendUrl = require('./appendUrl'); // module used for appending addressId to url

function fetchMunicipalities(urls){
  return Promise.all(urls.map(fetchBrgy));
}

function fetchBrgy(url) {
  return axios
    .get(url)
    .then((response)=> {
      return response.data.module;
    })
    .catch((error)=> {
      console.log(error);
    });
}


function provinceBrgy(startUrl ){
	axios.get(startUrl)
	.then(municipalities => {
		if (municipalities === null) {
			throw 'List of municipalities not Found';
		}

		// collect all request link for each municipality
		let fetchLinks = Array(0)
		municipalities.forEach((municipality) => {
			fetchLinks.push(`${startUrl}&addressId=${municipality.id}`)});

		fetchMunicipalities(fetchLinks)
			.then(resp=>{
				const flatten = [].concat(...resp);
				// const brgyList = 
				return flatten.map( ({ nameLocal, displayName, ...rest }) => rest );

				// return brgyList;
			})
			.catch(error=>{
				console.log(error)
			})
	})
	.catch(error => {
		console.log(error);
	});
}

function allBrgy(starturl){
	axios.get(starturl)
		.then(provinces => {
			if (provinces === null) {
				throw 'List of provinces not Found';
			}
			let links = Array(0);
			provinces.data.module.forEach((province)=>{
				links.push(`${starturl}&addressId=${province.id}`)
			});
			return links;
		})
		.then((links)=> {
			let brgy = Array(0);
			links.forEach((link)=> {
				brgy.push(provinceBrgy(link))
			})
			console.log(brgy) ;
		})
}

const province = 'Antique';
const municipality = 'Culasi';

const lazadaUrl =
	'https://member.lazada.com.ph/locationtree/api/getSubAddressList?countryCode=PH';

allBrgy(lazadaUrl);

// export
module.exports = allBrgy;