const axios = require('axios'); // module used for sending http requests
const path = require('path'); // module used for resolving directory-related queries
const { writeToPath } = require('@fast-csv/format'); // module used for exporting object array to csv format
const appendUrl = require('./appendUrl'); // module used for appending addressId to url

function fetchMunicipalities(urls){
  return Promise.all(urls.map(fetchBrgy));
}

function fetchBrgy(url) {
  return axios
    .get(url)
    .then((response)=> {
      return response.data.module;
    })
    .catch((error)=> {
      console.log(error);
    });
}


function provinceBrgy(startUrl, province ){
	axios.get(startUrl)
	.then(provinces => {
		if (provinces === null) {
			throw 'List of provinces not Found';
		}
		return axios.get(appendUrl(startUrl, provinces.data.module, province))
				.then((response) => response.data.module);
	})
	.then(municipalities => {
		if (municipalities === null) {
			throw 'List of municipalities not Found';
		}

		// collect all request link for each municipality
		let fetchLinks = Array(0)
		municipalities.forEach((municipality) => {
			fetchLinks.push(`${startUrl}&addressId=${municipality.id}`)});

		fetchMunicipalities(fetchLinks)
			.then(resp=>{
				const flatten = [].concat(...resp);
				const brgyList = flatten.map( ({ nameLocal, displayName, ...rest }) => rest );

				writeToPath(
					path.resolve(__dirname, '../output', `${province}-brgy-list.csv`),
					brgyList,
					{ headers: ['id', 'name', 'parentId'] }
				)
					.on('error', err => console.error(err))
					.on('finish', () => 
						console.log(`Done writing all brgy in ${province} to ${province}-brgy-list.csv.`)
					);
			})
			.catch(error=>{
				console.log(error)
			})
	})
	.catch(error => {
		console.log(error);
	});
}

const province = 'Antique';
const municipality = 'Culasi';

const lazadaUrl =
	'https://member.lazada.com.ph/locationtree/api/getSubAddressList?countryCode=PH';

// provinceBrgy(lazadaUrl, province);

// export
module.exports = provinceBrgy;