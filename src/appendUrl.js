/*
 * This function appends the addressId of the province/municipality, depending in the `place` param, to the `url` 
 * @param {string} url - The request URL to which we append the `addressId`
 * @param {object} data - The array of objects that contains the list of all provinces or municipalities in a province
 * @param {string} place - Either the name of municipality or province from which the barangay we desire to get belongs to
 * @return {string} - A request URL with the `addressId` of province/municipality appended to it
 */
function appendUrl(url, response, place) {
	const places = [...response]
	const placeDetails = places.filter(placeObj => placeObj.name.toLowerCase()                                                                                                 === place.toLowerCase())[0];
	if (placeDetails===undefined){throw `ID of ${place} is not found`;}
	const appendUrl = `${url}&addressId=${placeDetails.id}`;
	return appendUrl; 
}

module.exports = appendUrl;