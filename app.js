// Driver for getting barangay lists


// modules needed
const requestBrgy = require('./src/requestBrgy');
const promiseBrgy = require('./src/promiseBrgy');
const awaitBrgy = require('./src/awaitBrgy');
const provinceBrgy = require('./src/provinceBrgy');

const province = 'Antique';
const municipality = 'Culasi';

const lazadaUrl =
	'https://member.lazada.com.ph/locationtree/api/getSubAddressList?countryCode=PH';

//using request module
requestBrgy(lazadaUrl, province, municipality);

// Promise()
// promiseBrgy(lazadaUrl, province, municipality);

// async/await
// awaitBrgy(lazadaUrl, province, municipality);

// stretch goal 1
// provinceBrgy(lazadaUrl, province);


// stretch goal 2
