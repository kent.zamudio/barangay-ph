## Functions

<dl>
<dt><a href="#awaitBrgy">awaitBrgy(startUrl, [province], [municipality])</a> ⇒ <code>undefined</code></dt>
<dd><p>This async function sends the http request through the axios module, specifically the axios.get() function,
        it follows the sequence provinces-&gt;municipalities-&gt;barangays for each succeeding http request in order to get the list of brgy</p>
</dd>
<dt><a href="#promiseBrgy">promiseBrgy(startUrl, [province], [municipality])</a> ⇒ <code>Promise</code></dt>
<dd><p>This function sends the http request through the axios module, specifically the axios.get() function</p>
</dd>
<dt><a href="#getRequest">getRequest()</a> ⇒ <code>Object</code></dt>
<dd><p>This function sends the requests through the request module</p>
</dd>
<dt><a href="#requestBrgy">requestBrgy(url, [province], [municipality])</a> ⇒ <code>undefined</code></dt>
<dd><p>This function facilitates the whole process of sending requests and processing the response</p>
</dd>
<dt><del><a href="#axiosGetRequest">axiosGetRequest(url)</a> ⇒ <code>Promise</code></del></dt>
<dd></dd>
<dt><del><a href="#promiseBrgy_v1">promiseBrgy_v1(startUrl, [province], [municipality])</a> ⇒ <code>undefined</code></del></dt>
<dd></dd>
<dt><del><a href="#requestBrgy_v1">requestBrgy_v1(url, [province], [municipality])</a> ⇒ <code>undefined</code></del></dt>
<dd></dd>
</dl>

## Typedefs

<dl>
<dt><del><a href="#processResponse">processResponse</a> ⇒ <code>undefined</code></del></dt>
<dd></dd>
</dl>

<a name="awaitBrgy"></a>

## awaitBrgy(startUrl, [province], [municipality]) ⇒ <code>undefined</code>
This async function sends the http request through the axios module, specifically the axios.get() function,
		it follows the sequence provinces->municipalities->barangays for each succeeding http request in order to get the list of brgy

**Kind**: global function  

| Param | Type | Default | Description |
| --- | --- | --- | --- |
| startUrl | <code>string</code> |  | A request URL string that is needed for axios.get() function |
| [province] | <code>string</code> | <code>&quot;Antique&quot;</code> | the province from which the desired list of barangays belong to |
| [municipality] | <code>string</code> | <code>&quot;Culasi&quot;</code> | the municipality from which the desired list of barangays belong to |

<a name="promiseBrgy"></a>

## promiseBrgy(startUrl, [province], [municipality]) ⇒ <code>Promise</code>
This function sends the http request through the axios module, specifically the axios.get() function

**Kind**: global function  
**Returns**: <code>Promise</code> - - A promise object containing the array of provinces/municipalities/barangays depending on the sent param  

| Param | Type | Default | Description |
| --- | --- | --- | --- |
| startUrl | <code>string</code> |  | A request URL string that is needed for axios.get() function |
| [province] | <code>string</code> | <code>&quot;Antique&quot;</code> | the province from which the desired list of barangays belong to |
| [municipality] | <code>string</code> | <code>&quot;Culasi&quot;</code> | the municipality from which the desired list of barangays belong to |

<a name="getRequest"></a>

## getRequest() ⇒ <code>Object</code>
This function sends the requests through the request module

**Kind**: global function  
<a name="requestBrgy"></a>

## requestBrgy(url, [province], [municipality]) ⇒ <code>undefined</code>
This function facilitates the whole process of sending requests and processing the response

**Kind**: global function  

| Param | Type | Default | Description |
| --- | --- | --- | --- |
| url | <code>string</code> |  | The initial URL for crawling, the function appends `addressId` after each request calls |
| [province] | <code>string</code> | <code>&quot;Antique&quot;</code> | The province from which to the barangay belongs to |
| [municipality] | <code>string</code> | <code>&quot;Culasi&quot;</code> | The municipality from which to the barangay belongs to |

<a name="axiosGetRequest"></a>

## ~~axiosGetRequest(url) ⇒ <code>Promise</code>~~
***Deprecated***

**Kind**: global function  
**Returns**: <code>Promise</code> - - A promise object containing the array of provinces/municipalities/barangays depending on the sent param  

| Param | Type | Description |
| --- | --- | --- |
| url | <code>string</code> | A request URL string that is needed for axios.get() function |

<a name="promiseBrgy_v1"></a>

## ~~promiseBrgy\_v1(startUrl, [province], [municipality]) ⇒ <code>undefined</code>~~
***Deprecated***

**Kind**: global function  

| Param | Type | Default | Description |
| --- | --- | --- | --- |
| startUrl | <code>string</code> |  | the request URL for data crawling |
| [province] | <code>string</code> | <code>&quot;Antique&quot;</code> | the province from which the desired list of barangays belong to |
| [municipality] | <code>string</code> | <code>&quot;Culasi&quot;</code> | the municipality from which the desired list of barangays belong to |

<a name="requestBrgy_v1"></a>

## ~~requestBrgy\_v1(url, [province], [municipality]) ⇒ <code>undefined</code>~~
***Deprecated***

**Kind**: global function  

| Param | Type | Default | Description |
| --- | --- | --- | --- |
| url | <code>string</code> |  | The initial URL for crawling, the function appends `addressId` after each request calls |
| [province] | <code>string</code> | <code>&quot;Antique&quot;</code> | The province from which to the barangay belongs to |
| [municipality] | <code>string</code> | <code>&quot;Culasi&quot;</code> | The municipality from which to the barangay belongs to |

<a name="processResponse"></a>

## ~~processResponse ⇒ <code>undefined</code>~~
***Deprecated***

**Kind**: global typedef  

| Param | Type | Description |
| --- | --- | --- |
| error | <code>Error</code> | Contains the error that occured during the request process |
| body | <code>JSON</code> | The response of the request made |

